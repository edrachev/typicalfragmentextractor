package typicalfragmentextractor.text;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import typicalfragmentextractor.text.TextCompariosnCalculator;

public class TextSimilarityCalculatorTest {
	@Test
	public void testIsSequence() {
		Assert.assertTrue(TextCompariosnCalculator.isSequence(Arrays.asList("1", "2", "3"), Arrays.asList("1", "2", "3")));
		Assert.assertTrue(TextCompariosnCalculator.isSequence(Arrays.asList("1", "2", "3"), Arrays.asList("1", "2", "3", "4")));
		Assert.assertTrue(TextCompariosnCalculator.isSequence(Arrays.asList("2", "3"), Arrays.asList("1", "2", "3", "4")));
		
		Assert.assertFalse(TextCompariosnCalculator.isSequence(Arrays.asList("1", "1", "3"), Arrays.asList("1", "2", "3", "4")));
		Assert.assertFalse(TextCompariosnCalculator.isSequence(Arrays.asList("1", "3"), Arrays.asList("1", "2", "3", "4")));
	}
	
	@Test
	public void testCountTokensInSequences() {
		Assert.assertEquals(4, TextCompariosnCalculator.countTokensInSequences(Arrays.asList("1", "2", "3", "4", "5", "6"), Arrays.asList("2", "3", "1", "6", "4", "5")));
	}
}
