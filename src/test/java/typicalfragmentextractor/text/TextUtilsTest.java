package typicalfragmentextractor.text;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import typicalfragmentextractor.text.TextUtils;

public class TextUtilsTest {
	@Test
	public void testToWords() {
		Assert.assertEquals(Arrays.asList("����", "���", "���"), TextUtils.toWords("���� ��� ���"));
		Assert.assertEquals(Arrays.asList("one", "two", "three"), TextUtils.toWords("   one    two    three   "));
		Assert.assertEquals(Arrays.asList("one", "two", "three"), TextUtils.toWords(",,,   one    two,three,,,   "));
		Assert.assertEquals(Arrays.asList("one", "two", "three"), TextUtils.toWords(",,,   one,two.three,,,   "));
		Assert.assertEquals(Arrays.asList("one", "two", "three"), TextUtils.toWords(",,,   one-two:three,,,   "));
		Assert.assertEquals(Arrays.asList("one", "two", "three"), TextUtils.toWords(",,,   one(two)three,,,   "));
		Assert.assertEquals(Arrays.asList("one", "two", "three"), TextUtils.toWords(",,,   one\rtwo\nthree,,,   "));
		Assert.assertEquals(Arrays.asList("one", "two", "three"), TextUtils.toWords(",,,   one;two\nthree,,,   "));
		Assert.assertEquals(Arrays.asList("one", "two", "three"), TextUtils.toWords(",,,   one \"two\"three,,,   "));
		Assert.assertEquals(Arrays.asList("one", "two", "three"), TextUtils.toWords(",,,   one 'two'three,,,   "));
	}
	
	@Test
	public void toBaseTest() {
		Assert.assertEquals("����", TextUtils.toBase("������"));
	}
}
