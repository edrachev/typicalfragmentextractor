package typicalfragmentextractor;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import typicalfragmentextractor.environment.Environment;
import typicalfragmentextractor.environment.Settings;
import typicalfragmentextractor.persistance.entity.Fragment;
import typicalfragmentextractor.persistance.entity.TypicalFragment;
import typicalfragmentextractor.persistance.repository.EntityManagerProvider;
import typicalfragmentextractor.text.SynticallySplittedFragment;
import typicalfragmentextractor.text.TextCompariosnCalculator;

public class Model extends Observable implements Runnable {
	private static final Logger logger = LoggerFactory.getLogger(Model.class);
	
	private volatile State state = State.NEW;
	private Settings settings;
	private List<Fragment> unprocessedFragments;
	private List<TypicalFragment> typicalFragments;
	private List<SynticallySplittedFragment<TypicalFragment>> synticallySplittedTypicalFragments;
	private List<SynticallySplittedFragment<Fragment>> synticallySplittedFragments;
	private volatile TypicalFragment typicalFragment;
	private volatile List<SynticallySplittedFragment<Fragment>> fragmentsGroup;
	private EntityManager entityManager;
	private volatile float comparisonResult;
	private volatile int processed = 0;
	private volatile DifferentCharacteristics differentCharacteristics;

	
	public Model() {
	}

	@Override
	public void run() {
		logger.debug("run, model = {}", this);
		while (true) {
			switch (state) {
			case NEW:
				break;
			case INITIALIZING:
				logger.debug("INITIALIZING");
				try {
					entityManager = EntityManagerProvider.createEntityManager();
	
					unprocessedFragments = entityManager.createNamedQuery(Fragment.FIND_UNPROCESSED_FOR_TYPICAL, Fragment.class).getResultList();
					logger.debug("unprocessedFragments = {}", unprocessedFragments.size());
					synticallySplittedFragments = new ArrayList<>(unprocessedFragments.size());
					for (Fragment fragment : unprocessedFragments) {
						synticallySplittedFragments.add(new SynticallySplittedFragment<>(fragment));
					}
					
					typicalFragments = entityManager.createNamedQuery(TypicalFragment.READ_ALL, TypicalFragment.class).getResultList();
					logger.debug("typicalFragments = {}", typicalFragments.size());
					synticallySplittedTypicalFragments = new ArrayList<>();
					for (TypicalFragment fragment : typicalFragments) {
						synticallySplittedTypicalFragments.add(new SynticallySplittedFragment<>(fragment));
					}
					setState(State.RUNNING);
				} catch (Exception e) {
					logger.error("Initilization error", e);
					setState(State.ERROR);
				}
				break;
			case RUNNING:
				logger.debug("RUNNING");
				if (!synticallySplittedFragments.isEmpty()) {
					settings = Environment.readSettings();
					logger.debug("settings: {}", settings);
					SynticallySplittedFragment<Fragment> fragment = synticallySplittedFragments.get(0);
					logger.debug("fragment: {}", fragment);
					if(isAlreadyInTypical(fragment)) {
						logger.warn("Fragment is already in typical, not processed, fragment = {}", fragment);
						synticallySplittedFragments.remove(fragment);
						break;
					}
					
					List<SynticallySplittedFragment<Fragment>> group = findGroup(fragment);
					logger.debug("group: {}", group);
					if(group.size() < settings.getNumberFragmentsThreashold()) {
						logger.debug("number less then threashold");
						synticallySplittedFragments.removeAll(group);
						break;
					}
					
					if (areTextsAndCharacteristicsSame(group)) {
						logger.debug("text and characteristcs the same");
						List<Fragment> fragments = new ArrayList<>(group.size());
						try {
							entityManager.getTransaction().begin();
							for (SynticallySplittedFragment<Fragment> fragmentEl : group) {
								Fragment currentFragment = fragmentEl.getFragment();
								currentFragment.setProcessedForTypicalFragments(true);
								fragments.add(currentFragment);
							}

							typicalFragment = TypicalFragment.fromFragment(fragments.get(0));
							typicalFragments.add(typicalFragment);
							synticallySplittedTypicalFragments.add(new SynticallySplittedFragment<TypicalFragment>(typicalFragment));
							
							entityManager.persist(typicalFragment);

							entityManager.getTransaction().commit();

							synticallySplittedFragments.removeAll(group);
							processed += fragments.size();
							setState(State.RUNNING);
						} catch (Exception e) {
							logger.error("Problems at fragment saving", e);
							entityManager.getTransaction().rollback();
							setState(State.ERROR);
						}
					} else {
						logger.debug("have to show to expert");
						typicalFragment = TypicalFragment.fromFragment(group.get(0).getFragment());
						fragmentsGroup = new CopyOnWriteArrayList<>(group);
						comparisonResult = calculateMinComparisonResult(group);
						differentCharacteristics = calculateDifferentCharacteristics(group);
						logger.debug("to expert: {}", this);
						setState(State.WAITING_EXPERT);
					}
				}
				break;
			case STOPPED:
				break;
			case WAITING_EXPERT:
				break;

			default:
				break;
			}

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				logger.error("Multithreading error", e);
				throw new RuntimeException(e);
			}
		}
	}

	private static float calculateMinComparisonResult(List<SynticallySplittedFragment<Fragment>> group) {
		logger.debug("calculateMinComparisonResult: {}", group);
		float minComparisonResult = 1;
		Settings settings = Environment.readSettings();
		SynticallySplittedFragment<?> firstFragment = group.get(0);
		for(SynticallySplittedFragment<?> fragmentEl: group.subList(1, group.size())) {
			float comparisonResult = TextCompariosnCalculator.calculate(firstFragment, fragmentEl, settings);
			if(comparisonResult < minComparisonResult) {
				minComparisonResult = comparisonResult;
			}
		}
		
		logger.debug("minComparisonResult: {}", minComparisonResult);
		return minComparisonResult;
	}
	
	public void start() {
		logger.debug("start, model = {}", this);
		if (state == State.NEW) {
			setState(State.INITIALIZING);
		} else if (state == State.STOPPED) {
			setState(State.RUNNING);
		}
	}

	public void stop() {
		logger.debug("stop, model = {}", this);
		setState(State.STOPPED);
	}
	
	public void removeFragmentFromGroup(SynticallySplittedFragment<Fragment> fragment) {
		logger.debug("removeFragmentFromGroup: {}", fragment);
		fragmentsGroup.remove(fragment);
		setChanged();
		notifyObservers();
	}
	
	public void saveTypicalFragment() {
		logger.debug("saveTypicalFragment, model = {}", this);
		List<Fragment> fragments = new ArrayList<>(fragmentsGroup.size());
		try {
			entityManager.getTransaction().begin();
			
			for (SynticallySplittedFragment<Fragment> fragmentEl : fragmentsGroup) {
				Fragment fragment = fragmentEl.getFragment();
				fragment.setAct(typicalFragment.getAct());
				fragment.setQuality(typicalFragment.getQuality());
				fragment.setQuestion(typicalFragment.getQuestion());
				fragment.setWeight(typicalFragment.getWeight());
				fragment.setProcessedForTypicalFragments(true);
				fragments.add(fragment);
			}
			
			entityManager.persist(typicalFragment);
			
			typicalFragments.add(typicalFragment);
			synticallySplittedTypicalFragments.add(new SynticallySplittedFragment<TypicalFragment>(typicalFragment));

			synticallySplittedFragments.removeAll(fragmentsGroup);
			
			entityManager.getTransaction().commit();
			
			synticallySplittedFragments.removeAll(fragmentsGroup);
			
			processed += fragments.size();
			setState(State.RUNNING);
		} catch (Exception e) {
			logger.error("Problem at fragment saving", e);
			entityManager.getTransaction().rollback();
			setState(State.ERROR);
		}
	}
	
	private static boolean areTextsAndCharacteristicsSame(List<SynticallySplittedFragment<Fragment>> group) {
		logger.debug("areTextsAndCharacteristicsSame:", group);
		SynticallySplittedFragment<Fragment> fragment = group.get(0);
		for (SynticallySplittedFragment<Fragment> fragmentEl : group) {
			if (!fragment.getClearedText().equals(fragmentEl.getClearedText())) {
				logger.debug("result: false");
				return false;
			}
			
			if(!fragment.getFragment().getAct().equals(fragmentEl.getFragment().getAct())) {
				logger.debug("result: false");
				return false;
			}
			
			if(!fragment.getFragment().getQuality().equals(fragmentEl.getFragment().getQuality())) {
				logger.debug("result: false");
				return false;
			}
			
			if(!fragment.getFragment().getQuestion().equals(fragmentEl.getFragment().getQuestion())) {
				logger.debug("result: false");
				return false;
			}
			
			if(!fragment.getFragment().getWeight().equals(fragmentEl.getFragment().getWeight())) {
				logger.debug("result: false");
				return false;
			}
		}

		logger.debug("result: true");
		return true;
	}

	private static DifferentCharacteristics calculateDifferentCharacteristics(List<SynticallySplittedFragment<Fragment>> group) {
		logger.debug("calculateDifferentCharacteristics: {}", group);
		DifferentCharacteristics diffs = new DifferentCharacteristics();
		for(SynticallySplittedFragment<Fragment> outerFragment: group) {
			for(SynticallySplittedFragment<Fragment> innerFragment: group) {
				if(!outerFragment.getFragment().getAct().equals(innerFragment.getFragment().getAct())) {
					diffs.setAct(true);
				}
				
				if(!outerFragment.getFragment().getQuality().equals(innerFragment.getFragment().getQuality())) {
					diffs.setQuality(true);
				}
				
				if(!outerFragment.getFragment().getQuestion().equals(innerFragment.getFragment().getQuestion())) {
					diffs.setQuestion(true);
				}
				
				if(!outerFragment.getFragment().getWeight().equals(innerFragment.getFragment().getWeight())) {
					diffs.setWeight(true);
				}
				
				if(!outerFragment.getFragment().getQuestion().getPartition().equals(innerFragment.getFragment().getQuestion().getPartition())) {
					diffs.setPartition(true);
				}
			}
		}
		
		logger.debug("diffs: {}", diffs);
		return diffs;
	}
	
	private List<SynticallySplittedFragment<Fragment>> findGroup(SynticallySplittedFragment<Fragment> fragment) {
		logger.debug("findGroup: {}", fragment);
		List<SynticallySplittedFragment<Fragment>> fragmentGroup = new ArrayList<>();
		for (SynticallySplittedFragment<Fragment> fragmentEl : synticallySplittedFragments) {
			if (isFragmentsInGroup(fragmentEl, fragment)) {
				fragmentGroup.add(fragmentEl);
			}
		}

		logger.debug("fragmentGroup: {}", fragmentGroup);
		return fragmentGroup;
	}

	private boolean isAlreadyInTypical(SynticallySplittedFragment<Fragment> fragment) {
		logger.debug("isAlreadyInTypical: {}", fragment);
		for (SynticallySplittedFragment<?> fragmentEl : synticallySplittedTypicalFragments) {
			if (isFragmentsInGroup(fragmentEl, fragment)) {
				logger.debug("result: true");
				return true;
			}
		}

		logger.debug("result: false");
		return false;
	}
	
	private boolean isFragmentsInGroup(SynticallySplittedFragment<?> fragment1, SynticallySplittedFragment<?> fragment2) {
		logger.debug("isFragmentsInGroup: {} and {}", fragment1, fragment2);
		if (fragment1 == fragment2) {
			logger.debug("result: true");
			return true;
		} else if (fragment1.getClearedText().equals(fragment2.getClearedText())) {
			logger.debug("result: true");
			return true;
		}

		float comparisonResult = calculateComparisonResult(fragment1, fragment2);
		boolean result = comparisonResult >= settings.getThreadhold();
		logger.debug("result: {}", result);
		return result;
	}

	private float calculateComparisonResult(SynticallySplittedFragment<?> fragment1, SynticallySplittedFragment<?> fragment2) {
		logger.debug("calculateComparisonResult: {} and {}", fragment1, fragment2);
		float result = TextCompariosnCalculator.calculate(fragment1, fragment2, settings);
		logger.debug("result: {}", result);
		return result;
	}

	public TypicalFragment getTypicalFragment() {
		return typicalFragment;
	}
	public List<SynticallySplittedFragment<Fragment>> getFragmentsGroup() {
		return fragmentsGroup;
	}

	public State getState() {
		return state;
	}

	public float getComparisonResult() {
		return comparisonResult;
	}
	
	public int getProcessed() {
		return processed;
	}

	private void setState(State state) {
		logger.debug("setState: ", state);
		if (this.state != state) {
			this.state = state;
			setChanged();
			notifyObservers();
		}
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	public DifferentCharacteristics getDifferentCharacteristics() {
		return differentCharacteristics;
	}
	
	public static enum State {
		NEW,
		INITIALIZING,
		RUNNING,
		STOPPED,
		WAITING_EXPERT,
		ERROR
	}

	public static class DifferentCharacteristics {
		private boolean partition;
		private boolean question;
		private boolean act;
		private boolean quality;
		private boolean weight;
		
		public boolean isPartition() {
			return partition;
		}
		public void setPartition(boolean partition) {
			this.partition = partition;
		}
		public boolean isQuestion() {
			return question;
		}
		public void setQuestion(boolean question) {
			this.question = question;
		}
		public boolean isAct() {
			return act;
		}
		public void setAct(boolean act) {
			this.act = act;
		}
		public boolean isQuality() {
			return quality;
		}
		public void setQuality(boolean quality) {
			this.quality = quality;
		}
		public boolean isWeight() {
			return weight;
		}
		public void setWeight(boolean weight) {
			this.weight = weight;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (act ? 1231 : 1237);
			result = prime * result + (partition ? 1231 : 1237);
			result = prime * result + (quality ? 1231 : 1237);
			result = prime * result + (question ? 1231 : 1237);
			result = prime * result + (weight ? 1231 : 1237);
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DifferentCharacteristics other = (DifferentCharacteristics) obj;
			if (act != other.act)
				return false;
			if (partition != other.partition)
				return false;
			if (quality != other.quality)
				return false;
			if (question != other.question)
				return false;
			if (weight != other.weight)
				return false;
			return true;
		}
		@Override
		public String toString() {
			return "DifferentCharacteristics [partition=" + partition + ", question=" + question + ", act=" + act
					+ ", quality=" + quality + ", weight=" + weight + "]";
		}
	}

	@Override
	public String toString() {
		return "Model [state=" + nullOrToString(state) + ", settings=" + nullOrToString(settings) + ", unprocessedFragments.size()=" + countOrNull(unprocessedFragments)
				+ ", typicalFragments.size()=" + countOrNull(typicalFragments) + ", synticallySplittedTypicalFragments.size()="
				+ countOrNull(synticallySplittedTypicalFragments) + ", synticallySplittedFragments.size()=" + countOrNull(synticallySplittedFragments)
				+ ", typicalFragment=" + nullOrToString(typicalFragment) + ", fragmentsGroup=" + nullOrToString(fragmentsGroup) + ", entityManager="
				+ nullOrToString(entityManager) + ", comparisonResult=" + nullOrToString(comparisonResult) + ", processed=" + nullOrToString(processed)
				+ ", differentCharacteristics=" + nullOrToString(differentCharacteristics) + "]";
	}
	
	private static String nullOrToString(Object value) {
		if(value == null) {
			return "null";
		} else {
			return value.toString();
		}
	}
	
	private static String countOrNull(List<?> list) {
		if(list == null) {
			return "null";
		} else {
			return Integer.toString(list.size());
		}
	}
}
