package typicalfragmentextractor;

import java.io.File;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import typicalfragmentextractor.Model.State;
import typicalfragmentextractor.environment.Environment;
import typicalfragmentextractor.environment.Settings;
import typicalfragmentextractor.persistance.entity.Fragment;
import typicalfragmentextractor.text.SynticallySplittedFragment;
import typicalfragmentextractor.ui.FragmentControl;
import typicalfragmentextractor.ui.SettingsControl;
import typicalfragmentextractor.ui.TypicalFragmentControl;

public class Application extends javafx.application.Application {
	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	
	private static final int BUTTON_WIDTH = 100;
	private static final int WINDOW_WIDTH = 800;
	private static final int WINDOW_HEIGHT = 600;
	private static final String TITLE = "��������� ������� ����������";

	private final Model model = new Model();
	private final Button startStopButton = new Button("�����");
	private final ProgressBar progressBar = new ProgressBar();
	private final Text statusText = new Text();
	private final Text processedText = new Text();
	private final VBox fragmentsPane = new VBox();
	private final BorderPane typicalFragmentPane = new BorderPane();
	private Text comparisonResultTextbox = new Text();

	private Stage primaryStage;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		
		BorderPane border = new BorderPane();
		border.setTop(createTopPane());
		border.setCenter(createFragmentsPane());
		border.setBottom(createStatusBar());
		
		Scene scene = new Scene(border, WINDOW_WIDTH, WINDOW_HEIGHT);
		primaryStage.setScene(scene);
		primaryStage.setTitle(TITLE);
		primaryStage.show();
		
		render();
		
		model.addObserver(new Observer() {
			@Override
			public void update(Observable o, Object arg) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						render();
					}
				});
			}
		});
		
		Executors.newSingleThreadExecutor().execute(model);
	}
	
	private Pane createTopPane() {
		VBox topPane = new VBox();
		topPane.setFillWidth(true);
		
		MenuBar menuBar = new MenuBar();
		
		Menu databasePathMenu = new Menu("������� ���� ������");
		MenuItem databasePathMenuItem = new MenuItem("������� ���� ������");
		databasePathMenu.getItems().add(databasePathMenuItem);
		databasePathMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle("�������� ���� �����");
				File file = fileChooser.showOpenDialog(primaryStage);
				String path = file.getAbsolutePath();
				logger.debug("path: {}", path);
				Settings settings = Environment.readSettings();
				settings.setPathToDatabase(path);
				logger.debug("settings to save: {}", settings);
				Environment.saveSettings(settings);
			}
		});
		
		Menu settingsMenu = new Menu("���������");
		MenuItem settingsMenuItem = new MenuItem("���������");
		settingsMenu.getItems().add(settingsMenuItem);
		settingsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Stage stage = new Stage();
		        stage.setTitle("���������");
		        stage.setScene(new Scene(new SettingsControl(stage), 600, 230));
		        stage.show();
			}
		});
		
		menuBar.getMenus().addAll(databasePathMenu, settingsMenu);
		topPane.getChildren().add(menuBar);
		
		topPane.getChildren().add(createToolbar());
		topPane.getChildren().add(typicalFragmentPane);
		
		return topPane;
	}
	
	private Pane createToolbar() {
		HBox toolbar = new HBox();
		toolbar.setAlignment(Pos.CENTER_LEFT);
		toolbar.setPadding(new Insets(5));
		toolbar.setSpacing(10);
		startStopButton.setPrefWidth(BUTTON_WIDTH);
		startStopButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(model.getState() == State.RUNNING) {
					model.stop();
				} else if(model.getState() == State.NEW || model.getState() == State.STOPPED) {
					model.start();
				}
			}
		});
		toolbar.getChildren().add(startStopButton);
		comparisonResultTextbox.setTextAlignment(TextAlignment.CENTER);
		toolbar.getChildren().add(comparisonResultTextbox);
		return toolbar;
	}
	
	private Pane createStatusBar() {
		HBox statusBar = new HBox();
		statusBar.getChildren().add(processedText);
		Region spacer = new Region();
		HBox.setHgrow(spacer, Priority.ALWAYS);
		statusBar.getChildren().add(spacer);
		statusBar.getChildren().add(statusText);
		statusBar.getChildren().add(progressBar);
		statusBar.setAlignment(Pos.CENTER_RIGHT);
		statusBar.setPadding(new Insets(5));
		statusBar.setSpacing(10);
		return statusBar;
	}
	
	private Control createFragmentsPane() {
		fragmentsPane.setMaxWidth(Double.MAX_VALUE);
		fragmentsPane.setFillWidth(true);
		
		ScrollPane scroll = new ScrollPane();
		scroll.setHbarPolicy(ScrollBarPolicy.NEVER);
		scroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		scroll.setFitToWidth(true);
		scroll.setContent(fragmentsPane);
		
		return scroll;
	}

	private void render() {
		logger.debug("render, state: {}", model.getState());
		switch (model.getState()) {
		case NEW:
			renderNew();
			break;
		case INITIALIZING:
			renderInitializing();
			break;
		case RUNNING:
			renderRunning();
			break;
		case STOPPED:
			renderStopped();
			break;
		case WAITING_EXPERT:
			renderWaitingExpert();
			break;
		case ERROR:
			renderError();
			break;
		default:
			String message = "Unsupported state: " + model.getState();
			logger.error(message);
			throw new RuntimeException(message);
		}
	}
	
	private void renderNew() {
		logger.debug("renderNew, model = {}", model);
		statusText.setText("������");
		startStopButton.setText("�����");
		processedText.setText("���������� �� �����: " + model.getProcessed());
		progressBar.setVisible(false);
		comparisonResultTextbox.setVisible(false);
	}
	
	private void renderRunning() {
		logger.debug("renderRunning, model = {}", model);
		statusText.setText("��� ���������");
		startStopButton.setText("����");
		processedText.setText("���������� �� �����: " + model.getProcessed());
		startStopButton.setDisable(false);
		progressBar.setVisible(true);
		comparisonResultTextbox.setVisible(false);
		fragmentsPane.getChildren().clear();
		typicalFragmentPane.getChildren().clear();
	}
	
	private void renderInitializing() {
		logger.debug("renderInitializing, model = {}", model);
		statusText.setText("�������������");
		processedText.setText("���������� �� �����: " + model.getProcessed());
		startStopButton.setDisable(true);
		progressBar.setVisible(true);
	}
	
	private void renderStopped() {
		logger.debug("renderStopped, model = {}", model);
		statusText.setText("�����������");
		processedText.setText("���������� �� �����: " + model.getProcessed());
		startStopButton.setDisable(false);
		progressBar.setVisible(false);
	}
	
	private void renderWaitingExpert() {
		logger.debug("renderWaitingExpert, model = {}", model);
		statusText.setText("�������� ��������");
		processedText.setText("���������� �� �����: " + model.getProcessed());
		startStopButton.setDisable(true);
		progressBar.setVisible(false);
		comparisonResultTextbox.setVisible(true);
		String comparisonResultMessage = String.format("���������� ����������: %3.1f%%", model.getComparisonResult() * 100);
		comparisonResultTextbox.setText(comparisonResultMessage);
		fragmentsPane.getChildren().clear();
		typicalFragmentPane.getChildren().clear();
		
		typicalFragmentPane.setCenter(new TypicalFragmentControl(model.getTypicalFragment(), model));
		
		for(SynticallySplittedFragment<Fragment> fragmentEl: model.getFragmentsGroup()) {
			fragmentsPane.getChildren().add(new FragmentControl(fragmentEl, model));
		}
	}
	
	private void renderError() {
		logger.debug("renderError, model = {}", model);
		statusText.setText("������, ���������� � ������������");
		processedText.setText("���������� �� �����: " + model.getProcessed());
		startStopButton.setDisable(false);
		progressBar.setVisible(false);
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
