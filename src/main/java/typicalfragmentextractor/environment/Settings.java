package typicalfragmentextractor.environment;

import java.util.Properties;

public class Settings {
	private static final String PATH_TO_DATABASE = "pathToDatabase";
	private static final String THREASHOLD = "threashold";
	private static final String SAME_WORD_COEFFICIENT = "sameWordCoefficient";
	private static final String SAME_BASE_COEFFICIENT = "sameBaseCoefficient";
	private static final String WORD_SEQUENCE_COEFFICIENT = "wordSequenceCoefficient";
	private static final String NUMBER_FRAGMENT_THREASHOLD = "numberFragmentsThreashold";
	
	
	private String pathToDatabase;
	private float threadhold;
	private float sameWordCoefficient;
	private float sameBaseCoefficient;
	private float wordSequenceCoefficient;
	private int numberFragmentsThreashold;
	
	public static final Settings DEFAULT = new Settings("c://pathToYourDatabase//base.mdb", 0.7f, 3.0f, 0.3f, 0.6f, 1);
	
	public static Settings fromProperties(Properties properties) {
		Settings settings = new Settings();
		settings.setPathToDatabase(properties.getProperty(PATH_TO_DATABASE, "c://pathToYourDatabase//base.mdb"));
		settings.setThreadhold(Float.parseFloat(properties.getProperty(THREASHOLD, "0")));
		settings.setSameWordCoefficient(Float.parseFloat(properties.getProperty(SAME_WORD_COEFFICIENT, "0")));
		settings.setSameBaseCoefficient(Float.parseFloat(properties.getProperty(SAME_BASE_COEFFICIENT, "0")));
		settings.setWordSequenceCoefficient(Float.parseFloat(properties.getProperty(WORD_SEQUENCE_COEFFICIENT, "0")));
		settings.setNumberFragmentsThreashold(Integer.parseInt(properties.getProperty(NUMBER_FRAGMENT_THREASHOLD, "1")));
		return settings;
	}
	
	public Settings() {}
	public Settings(String pathToDatabase, float threadhold, float sameWordCoefficient, float sameBaseCoefficient, float wordSequenceCoefficient, int numberFragmentsThreashold) {
		this.pathToDatabase = pathToDatabase;
		this.threadhold = threadhold;
		this.sameWordCoefficient = sameWordCoefficient;
		this.sameBaseCoefficient = sameBaseCoefficient;
		this.wordSequenceCoefficient = wordSequenceCoefficient;
		this.numberFragmentsThreashold = numberFragmentsThreashold;
	}
	
	public String getPathToDatabase() {
		return pathToDatabase;
	}
	public void setPathToDatabase(String pathToDatabase) {
		this.pathToDatabase = pathToDatabase;
	}
	public float getThreadhold() {
		return threadhold;
	}
	public void setThreadhold(float threadhold) {
		this.threadhold = threadhold;
	}
	public float getSameWordCoefficient() {
		return sameWordCoefficient;
	}
	public void setSameWordCoefficient(float sameWordCoefficient) {
		this.sameWordCoefficient = sameWordCoefficient;
	}
	public float getSameBaseCoefficient() {
		return sameBaseCoefficient;
	}
	public void setSameBaseCoefficient(float sameBaseCoefficient) {
		this.sameBaseCoefficient = sameBaseCoefficient;
	}
	public float getWordSequenceCoefficient() {
		return wordSequenceCoefficient;
	}
	public void setWordSequenceCoefficient(float wordSequenceCoefficient) {
		this.wordSequenceCoefficient = wordSequenceCoefficient;
	}
	public int getNumberFragmentsThreashold() {
		return numberFragmentsThreashold;
	}
	public void setNumberFragmentsThreashold(int numberFragmentsThreashold) {
		this.numberFragmentsThreashold = numberFragmentsThreashold;
	}
	public Properties toProperties() {
		Properties properties = new Properties();
		properties.setProperty(PATH_TO_DATABASE, pathToDatabase);
		properties.setProperty(THREASHOLD, Float.toString(threadhold));
		properties.setProperty(SAME_WORD_COEFFICIENT, Float.toString(sameWordCoefficient));
		properties.setProperty(SAME_BASE_COEFFICIENT, Float.toString(sameBaseCoefficient));
		properties.setProperty(WORD_SEQUENCE_COEFFICIENT, Float.toString(wordSequenceCoefficient));
		properties.setProperty(NUMBER_FRAGMENT_THREASHOLD, Integer.toString(numberFragmentsThreashold));
		return properties;
	}

	@Override
	public String toString() {
		return "Settings [pathToDatabase=" + pathToDatabase + ", threadhold=" + threadhold + ", sameWordCoefficient="
				+ sameWordCoefficient + ", sameBaseCoefficient=" + sameBaseCoefficient + ", wordSequenceCoefficient="
				+ wordSequenceCoefficient + ", numberFragmentsThreashold=" + numberFragmentsThreashold + "]";
	}
}
