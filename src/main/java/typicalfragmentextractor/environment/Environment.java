package typicalfragmentextractor.environment;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Environment {
	private static final String SETTINGS_PROPERTIES = "settings.properties";
	
	private static Logger logger = LoggerFactory.getLogger(Environment.class);
	
	public static Settings readSettings() {
		Properties properties = new Properties();
		Settings settings;
		try {
			properties.load(Files.newInputStream(Paths.get(SETTINGS_PROPERTIES)));
			logger.debug("properties: {}", properties);
			settings = Settings.fromProperties(properties);
			logger.info("Read settings: {}", settings);
		} catch (IOException e) {
			logger.error("Cannot read settings.properties");
			settings = Settings.DEFAULT;
			saveSettings(settings);
		}
		return settings;
	}
	
	public static void saveSettings(Settings settings) {
		Properties properties = settings.toProperties();
		try {
			logger.info("Save settings: {}", settings);
			properties.store(Files.newOutputStream(Paths.get(SETTINGS_PROPERTIES)), "Created by TypicalFragmentExtractor");
			logger.debug("properties: {}", properties);
		} catch (IOException e) {
			logger.error("Cannot save settings.properties");
			throw new RuntimeException("Cannot save settings", e);
		}
	}
}
