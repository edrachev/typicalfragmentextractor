package typicalfragmentextractor.persistance.repository;

import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import typicalfragmentextractor.environment.Environment;
import typicalfragmentextractor.environment.Settings;

public class EntityManagerProvider {
	private final static Logger logger = LoggerFactory.getLogger(EntityManagerProvider.class);

	public static EntityManager createEntityManager() {
		Settings settings = Environment.readSettings();

		Properties properties = new Properties();
		String connectionUrl = "jdbc:ucanaccess://" + settings.getPathToDatabase() + ";memory=false";
		logger.info("try to create EntitiyManager for connection string: {}", connectionUrl);
		properties.put("javax.persistence.jdbc.url", connectionUrl);
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("FragmentsManagement",
				properties);
		return entityManagerFactory.createEntityManager();
	}
}
