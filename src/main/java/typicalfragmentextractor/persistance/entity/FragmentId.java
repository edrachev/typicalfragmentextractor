package typicalfragmentextractor.persistance.entity;

public class FragmentId {
	private Long textNum;
	private String contractNum;
	private String text;
	
	public FragmentId() {
	}
	
	public FragmentId(Long textNum, String contractNum) {
		this.textNum = textNum;
		this.contractNum = contractNum;
	}
	
	public Long getTextNum() {
		return textNum;
	}
	public void setTextNum(Long textNum) {
		this.textNum = textNum;
	}
	public String getContractNum() {
		return contractNum;
	}
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contractNum == null) ? 0 : contractNum.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((textNum == null) ? 0 : textNum.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FragmentId other = (FragmentId) obj;
		if (contractNum == null) {
			if (other.contractNum != null)
				return false;
		} else if (!contractNum.equals(other.contractNum))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (textNum == null) {
			if (other.textNum != null)
				return false;
		} else if (!textNum.equals(other.textNum))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FragmentId [textNum=" + textNum + ", contractNum=" + contractNum + ", text=" + text + "]";
	}
}
