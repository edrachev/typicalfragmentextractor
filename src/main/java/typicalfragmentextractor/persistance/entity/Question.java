package typicalfragmentextractor.persistance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "�������")
@NamedQuery(name = Question.READ_ALL, query = "SELECT q FROM Question q")
public class Question {
	public static final String READ_ALL = "Question.readAll";
	
	@Id
	@Column(name = "���")
	private Long id;
	@Column(name = "������")
	private String code;
	
	@JoinColumn(name = "������")
	@ManyToOne
	private Partition partition;
	
	@Column(name = "�����������")
	private String descriotion;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Partition getPartition() {
		return partition;
	}
	public void setPartition(Partition partition) {
		this.partition = partition;
	}
	public String getDescriotion() {
		return descriotion;
	}
	public void setDescriotion(String descriotion) {
		this.descriotion = descriotion;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((descriotion == null) ? 0 : descriotion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((partition == null) ? 0 : partition.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Question other = (Question) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (descriotion == null) {
			if (other.descriotion != null)
				return false;
		} else if (!descriotion.equals(other.descriotion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (partition == null) {
			if (other.partition != null)
				return false;
		} else if (!partition.equals(other.partition))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Question [id=" + id + ", code=" + code + ", partition=" + partition.getCode() + ", descriotion=" + descriotion
				+ "]";
	}
	
}
