package typicalfragmentextractor.persistance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import typicalfragmentextractor.text.TextContainer;

@Entity
@Table(name = "����������������")
@NamedQuery(name = TypicalFragment.READ_ALL, query = "SELECT f FROM TypicalFragment f")
@TableGenerator(name="SEQUENCE")
public class TypicalFragment implements TextContainer {
	public static final String READ_ALL = "TypicalFragment.readAll";
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SEQUENCE")
	@Column(name = "Id")
	private volatile Long id;
		
	@Column(name = "�����")
	private volatile String text;
	
	@Column(name = "����������")
	private volatile Float weight;
	
	@JoinColumn(name = "��������")
	@ManyToOne
	private volatile Quality quality;
	
	@JoinColumn(name = "���")
	@ManyToOne
	private volatile Act act;
	
	@JoinColumn(name = "������")
	@ManyToOne
	private volatile Question question;
	
	
	public static TypicalFragment fromFragment(Fragment fragment) {
		TypicalFragment typicalFragment = new TypicalFragment();
		typicalFragment.text = fragment.getText();
		typicalFragment.weight = fragment.getWeight();
		typicalFragment.quality = fragment.getQuality();
		typicalFragment.act = fragment.getAct();
		typicalFragment.question = fragment.getQuestion();
		return typicalFragment;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Float getWeight() {
		return weight;
	}
	public void setWeight(Float weight) {
		this.weight = weight;
	}
	public Quality getQuality() {
		return quality;
	}
	public void setQuality(Quality quality) {
		this.quality = quality;
	}
	public Act getAct() {
		return act;
	}
	public void setAct(Act act) {
		this.act = act;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((act == null) ? 0 : act.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((quality == null) ? 0 : quality.hashCode());
		result = prime * result + ((question == null) ? 0 : question.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((weight == null) ? 0 : weight.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypicalFragment other = (TypicalFragment) obj;
		if (act == null) {
			if (other.act != null)
				return false;
		} else if (!act.equals(other.act))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (quality == null) {
			if (other.quality != null)
				return false;
		} else if (!quality.equals(other.quality))
			return false;
		if (question == null) {
			if (other.question != null)
				return false;
		} else if (!question.equals(other.question))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TypicalFragment [id=" + id + ", text=" + text + ", weight=" + weight + ", quality=" + quality + ", act="
				+ act + ", question=" + question + "]";
	}
	
}
