package typicalfragmentextractor.persistance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import typicalfragmentextractor.text.TextContainer;

@Entity
@IdClass(FragmentId.class)
@Table(name = "������")
@NamedQueries({
	@NamedQuery(name = Fragment.READ_ALL, query = "SELECT f FROM Fragment f"),
	@NamedQuery(name = Fragment.FIND_UNPROCESSED_FOR_TYPICAL, query = "SELECT f FROM Fragment f WHERE f.processedForTypicalFragments = FALSE")
})
public class Fragment implements TextContainer {
	public static final String READ_ALL = "Fragment.readAll";
	public static final String FIND_UNPROCESSED_FOR_TYPICAL = "Fragment.findUnprocessedForTypical";

	@Id
	@Column(name = "[#�����]")
	private Long textNum;
	
	@Id
	@Column(name = "[#���]")
	private String contractNum;
	
	@Id
	@Column(name = "�����")
	private String text;
	
	@Column(name = "����������")
	private Float weight;
	
	@JoinColumn(name = "��������")
	@ManyToOne
	private Quality quality;
	
	@JoinColumn(name = "���")
	@ManyToOne
	private Act act;
	
	@JoinColumn(name = "������")
	@ManyToOne
	private Question question;
	
	@Column(name = "�����������")
	private String includeToHelp;
	
	@Column(name = "������������������������")
	private boolean processedForOriginality;
	
	@Column(name = "��������������������")
	private boolean processedForTypicalFragments;

	public Long getTextNum() {
		return textNum;
	}
	public void setTextNum(Long textNum) {
		this.textNum = textNum;
	}
	public String getContractNum() {
		return contractNum;
	}
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Float getWeight() {
		return weight;
	}
	public void setWeight(Float weight) {
		this.weight = weight;
	}
	public Quality getQuality() {
		return quality;
	}
	public void setQuality(Quality quality) {
		this.quality = quality;
	}
	public Act getAct() {
		return act;
	}
	public void setAct(Act act) {
		this.act = act;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public String getIncludeToHelp() {
		return includeToHelp;
	}
	public void setIncludeToHelp(String includeToHelp) {
		this.includeToHelp = includeToHelp;
	}
	public boolean getProcessedForOriginality() {
		return processedForOriginality;
	}
	public void setProcessedForOriginality(boolean processedForOriginality) {
		this.processedForOriginality = processedForOriginality;
	}
	public boolean getProcessedForTypicalFragments() {
		return processedForTypicalFragments;
	}
	public void setProcessedForTypicalFragments(boolean processedForTypicalFragments) {
		this.processedForTypicalFragments = processedForTypicalFragments;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((act == null) ? 0 : act.hashCode());
		result = prime * result + ((contractNum == null) ? 0 : contractNum.hashCode());
		result = prime * result + ((includeToHelp == null) ? 0 : includeToHelp.hashCode());
		result = prime * result + (processedForOriginality ? 1231 : 1237);
		result = prime * result + (processedForTypicalFragments ? 1231 : 1237);
		result = prime * result + ((quality == null) ? 0 : quality.hashCode());
		result = prime * result + ((question == null) ? 0 : question.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((textNum == null) ? 0 : textNum.hashCode());
		result = prime * result + ((weight == null) ? 0 : weight.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fragment other = (Fragment) obj;
		if (act == null) {
			if (other.act != null)
				return false;
		} else if (!act.equals(other.act))
			return false;
		if (contractNum == null) {
			if (other.contractNum != null)
				return false;
		} else if (!contractNum.equals(other.contractNum))
			return false;
		if (includeToHelp == null) {
			if (other.includeToHelp != null)
				return false;
		} else if (!includeToHelp.equals(other.includeToHelp))
			return false;
		if (processedForOriginality != other.processedForOriginality)
			return false;
		if (processedForTypicalFragments != other.processedForTypicalFragments)
			return false;
		if (quality == null) {
			if (other.quality != null)
				return false;
		} else if (!quality.equals(other.quality))
			return false;
		if (question == null) {
			if (other.question != null)
				return false;
		} else if (!question.equals(other.question))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (textNum == null) {
			if (other.textNum != null)
				return false;
		} else if (!textNum.equals(other.textNum))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Fragment [textNum=" + textNum + ", contractNum=" + contractNum + ", text=" + text + ", weight=" + weight
				+ ", quality=" + quality + ", act=" + act + ", question=" + question + ", includeToHelp="
				+ includeToHelp + ", processedForOriginality=" + processedForOriginality
				+ ", processedForTypicalFragments=" + processedForTypicalFragments + "]";
	}
}
