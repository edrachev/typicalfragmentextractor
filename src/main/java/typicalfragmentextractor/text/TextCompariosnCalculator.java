package typicalfragmentextractor.text;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import typicalfragmentextractor.environment.Settings;

public class TextCompariosnCalculator {
	private static final Logger logger = LoggerFactory.getLogger(TextCompariosnCalculator.class);
	
	public static float calculate(SynticallySplittedFragment<?> fragment1, SynticallySplittedFragment<?> fragment2,
			Settings settings) {
		logger.debug("Calculate for {} and {}", fragment1, fragment2);
		int sameWords = countSameTokens(fragment1.getWords(), fragment2.getWords());
		logger.debug("sameWords: {}", sameWords);
		int sameBases = countSameTokens(fragment1.getBases(), fragment2.getBases());
		logger.debug("sameBases: {}", sameBases);
		int basesInSequences = countTokensInSequences(fragment1.getBases(), fragment2.getBases());
		logger.debug("basesInSequences: {}", basesInSequences);

		int numWords = Math.max(fragment1.getWords().size(), fragment2.getWords().size());

		logger.debug("numWords: {}", numWords);
		float sameWordsPercent = sameWords / (float) numWords;
		logger.debug("sameWordsPercent: {}", sameWordsPercent);
		float sameBasesPercent = sameBases / (float) numWords;
		logger.debug("sameBasesPercent: {}", sameBasesPercent);
		float basesInSequencesPercent = basesInSequences / (float) numWords;
		logger.debug("basesInSequencesPercent: {}", basesInSequencesPercent);

		float result = (sameWordsPercent * settings.getSameWordCoefficient()
				+ sameBasesPercent * settings.getSameBaseCoefficient()
				+ basesInSequencesPercent * settings.getWordSequenceCoefficient())
				/ (settings.getSameWordCoefficient() + settings.getSameBaseCoefficient()
						+ settings.getWordSequenceCoefficient());
		logger.debug("result: {}", result);
		return result;
	}

	private static int countSameTokens(List<String> tokens1, List<String> tokens2) {
		logger.debug("countSameTokens: {} and {}", tokens1, tokens2);
		int count = 0;
		cycle1: for (String token1El : tokens1) {
			for (String token2El : tokens2) {
				if (token1El.equals(token2El)) {
					count++;
					continue cycle1;
				}
			}
		}

		logger.debug("count: {}", count);
		return count;
	}

	static int countTokensInSequences(List<String> tokens1, List<String> tokens2) {
		logger.debug("countTokensInSequences: {} and {}", tokens1, tokens2);
		int count = 0;
		for (int i = 0; i < tokens1.size(); i++) {
			if (i != 0) {
				if (isSequence(tokens1.subList(i - 1, i + 1), tokens2)) {
					count++;
					continue;
				}
			}

			if (i != tokens1.size() - 1) {
				if (isSequence(tokens1.subList(i, i + 2), tokens2)) {
					count++;
					continue;
				}
			}
		}

		logger.debug("count: {}", count);
		return count;
	}

	static boolean isSequence(List<String> testSequence, List<String> tokens) {
		logger.debug("isSequence: testSequence = {}, tokens = {}", testSequence, tokens);
		int sequenceSize = testSequence.size();
		for (int i = 0; i < tokens.size() - sequenceSize + 1; i++) {
			if (tokens.subList(i, i + sequenceSize).equals(testSequence)) {
				logger.debug("result: true");
				return true;
			}
		}

		logger.debug("result: false");
		return false;
	}
}
