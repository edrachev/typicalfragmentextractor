package typicalfragmentextractor.text;

import java.util.List;

public class SynticallySplittedFragment<T extends TextContainer> {
	private final T fragment;
	private final List<String> words;
	private final List<String> bases;
	private final String clearedText;
	
	public SynticallySplittedFragment(T fragment) {
		this.fragment = fragment;
		
		words = TextUtils.toWords(fragment.getText());
		clearedText = String.join(" ", words.toArray(new String[words.size()]));
		bases = TextUtils.toBases(words);
	}

	public T getFragment() {
		return fragment;
	}
	public List<String> getWords() {
		return words;
	}
	public List<String> getBases() {
		return bases;
	}
	public String getClearedText() {
		return clearedText;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bases == null) ? 0 : bases.hashCode());
		result = prime * result + ((clearedText == null) ? 0 : clearedText.hashCode());
		result = prime * result + ((fragment == null) ? 0 : fragment.hashCode());
		result = prime * result + ((words == null) ? 0 : words.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("rawtypes")
		SynticallySplittedFragment other = (SynticallySplittedFragment) obj;
		if (bases == null) {
			if (other.bases != null)
				return false;
		} else if (!bases.equals(other.bases))
			return false;
		if (clearedText == null) {
			if (other.clearedText != null)
				return false;
		} else if (!clearedText.equals(other.clearedText))
			return false;
		if (fragment == null) {
			if (other.fragment != null)
				return false;
		} else if (!fragment.equals(other.fragment))
			return false;
		if (words == null) {
			if (other.words != null)
				return false;
		} else if (!words.equals(other.words))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SynticallySplittedFragment [fragment=" + fragment + ", words=" + words + ", bases=" + bases
				+ ", clearedText=" + clearedText + "]";
	}
}
