package typicalfragmentextractor.text;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TextPreprocesser {
	private static final Logger logger = LoggerFactory.getLogger(TextPreprocesser.class);
	
	public static String preprocess(String text) {
		logger.debug("preprocess: {}", text);
		StringBuilder builder = new StringBuilder(text.trim().toLowerCase());
		while (!Character.isLetter(builder.charAt(0))) {
			builder.deleteCharAt(0);
		}
		String result = builder.toString();
		logger.debug("result: {}", result);
		return result;
	}
}
