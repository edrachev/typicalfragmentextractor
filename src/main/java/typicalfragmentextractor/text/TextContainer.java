package typicalfragmentextractor.text;

public interface TextContainer {
	String getText();
}
