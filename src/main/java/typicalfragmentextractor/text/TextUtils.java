package typicalfragmentextractor.text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TextUtils {
	private static final Logger logger = LoggerFactory.getLogger(TextUtils.class);
	
	private static final String DELIMITERS = " |,|\\.|-|:|\\(|\\)|;|\r|\n|\"|'";
	private static final String[] ENDINGS = new String[] { "��", "���", "���", "���", "���", "���", "���", "���", "���",
			"���", "���", "���", "���", "���", "���", "���", "���", "��", "��", "��", "��", "��", "��", "��", "��",
			"��", "��", "��", "��", "��", "��", "��", "��", "��", "��", "��", "��", "��", "��", "��", "��", "��", "��",
			"��", "��", "��", "��", "��", "��", "��", "��", "��", "��", "�", "�", "�", "�", "�", "�", "�", "�", "�" };

	public static List<String> toWords(String text) {
		logger.debug("toWords: {}", text);
		String preprocessedText = TextPreprocesser.preprocess(text);
		List<String> result = new LinkedList<>(Arrays.asList(preprocessedText.split(DELIMITERS)));
		ListIterator<String> iterator = result.listIterator();
		while (iterator.hasNext()) {
			String word = iterator.next();
			if (!isWordOrDigit(word)) {
				iterator.remove();
			}
		}
		
		logger.debug("result: {}", result);
		return result;
	}

	public static List<String> toBases(List<String> words) {
		logger.debug("toBases: {}", words);
		List<String> bases = new ArrayList<>(words.size());
		for(String word: words) {
			bases.add(word);
		}
		logger.debug("bases: {}", bases);
		return bases;
	}
	
	private static boolean isWordOrDigit(String word) {
		logger.debug("isWordOrDigit: {}", word);
		if (word.length() == 0) {
			return false;
		}
		boolean result = Character.isLetterOrDigit(word.charAt(0));
		logger.debug("result: {}", result);
		return result;
	}


	static String toBase(String word) {
		logger.debug("toBase: {}", word);
		for (String ending : ENDINGS) {
			if(word.endsWith(ending)) {
				if(word.equals(ending)) {
					return word;
				} else {
					return word.substring(0, word.length() - ending.length());
				}
			}
		}
		
		logger.debug("result: {}", word);
		return word;
	}
}
