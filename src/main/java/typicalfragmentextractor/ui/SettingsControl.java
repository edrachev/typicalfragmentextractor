package typicalfragmentextractor.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import typicalfragmentextractor.environment.Environment;
import typicalfragmentextractor.environment.Settings;

public class SettingsControl extends GridPane {
	private static final Logger logger = LoggerFactory.getLogger(SettingsControl.class);
	
	private final TextField sameWordCoefficientTextfield;
	private final TextField sameBaseCoefficientTextfield;
	private final TextField wordSequenceCoefficientTextfield;
	private final TextField threadholdTextfield;
	private final TextField numberFragmentsThreadholdTextfield;
	
	public SettingsControl(Stage stage) {
		this.setAlignment(Pos.CENTER);
		this.setPadding(new Insets(5));
		this.setHgap(5);
		this.setVgap(5);
		
		
		Settings settings = Environment.readSettings();
		logger.debug("Read settings: {}", settings);
		
		this.add(new Label("����������� ���������� ����"), 0, 0);
		sameWordCoefficientTextfield = new TextField(Float.toString(settings.getSameWordCoefficient()));
		this.add(sameWordCoefficientTextfield, 1, 0);
		
		this.add(new Label("����������� ���������� �����"), 0, 1);
		sameBaseCoefficientTextfield = new TextField(Float.toString(settings.getSameBaseCoefficient()));
		this.add(sameBaseCoefficientTextfield, 1, 1);
		
		this.add(new Label("����������� ���������� �������������������"), 0, 2);
		wordSequenceCoefficientTextfield = new TextField(Float.toString(settings.getWordSequenceCoefficient()));
		this.add(wordSequenceCoefficientTextfield, 1, 2);
		
		this.add(new Label("����� ������������ ������� ����������"), 0, 3);
		threadholdTextfield = new TextField(Float.toString(settings.getThreadhold()));
		this.add(threadholdTextfield, 1, 3);
		
		this.add(new Label("����� ����������� ������� ����������"), 0, 4);
		numberFragmentsThreadholdTextfield = new TextField(Integer.toString(settings.getNumberFragmentsThreashold()));
		this.add(numberFragmentsThreadholdTextfield, 1, 4);
		
		Button button = new Button("���������");
		button.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				settings.setSameWordCoefficient(Float.parseFloat(sameWordCoefficientTextfield.getText()));
				settings.setSameBaseCoefficient(Float.parseFloat(sameBaseCoefficientTextfield.getText()));
				settings.setWordSequenceCoefficient(Float.parseFloat(wordSequenceCoefficientTextfield.getText()));
				settings.setThreadhold(Float.parseFloat(threadholdTextfield.getText()));
				settings.setNumberFragmentsThreashold(Integer.parseInt(numberFragmentsThreadholdTextfield.getText()));
				logger.debug("Settings to save: {}", settings);
				Environment.saveSettings(settings);
				stage.close();
			}
		});
		
		GridPane.setHalignment(button, HPos.CENTER);
		this.add(button, 0, 5, 2, 1);
	}
}
