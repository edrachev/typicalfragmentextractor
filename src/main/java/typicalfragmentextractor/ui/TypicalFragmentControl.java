package typicalfragmentextractor.ui;



import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import typicalfragmentextractor.Model;
import typicalfragmentextractor.Model.DifferentCharacteristics;
import typicalfragmentextractor.persistance.entity.Act;
import typicalfragmentextractor.persistance.entity.Partition;
import typicalfragmentextractor.persistance.entity.Quality;
import typicalfragmentextractor.persistance.entity.Question;
import typicalfragmentextractor.persistance.entity.TypicalFragment;

public class TypicalFragmentControl extends BorderPane {
	private static final Logger logger = LoggerFactory.getLogger(TypicalFragment.class);
	
	private static final int RIGHT_PANEL_WIDTH = 430;
	private static final Color FILL_COLOR = Color.BLUE;
	
	private final TypicalFragment typicalFragment;
	private final Model model;
	private final EntityManager entityManager;
	
	private final TextArea textArea = new TextArea();
	private final ComboBox<Partition> partitionCombobox = new ComboBox<>();
	private final ComboBox<Question> questionCombobox = new ComboBox<>();
	private final ComboBox<Act> actCombobox = new ComboBox<>();
	private final ComboBox<Quality> qualityCombobox = new ComboBox<>();
	private final ComboBox<Float> weightCombobox = new ComboBox<>();
	private final Button saveButton = new Button("��������� ������� ��������");
	
	public TypicalFragmentControl(TypicalFragment typicalFragment, Model model) {
		logger.debug("TypicalFragmentControl typicalFragment = {}, model = {}", typicalFragment, model);
		this.typicalFragment = typicalFragment;
		this.model = model;
		this.entityManager = model.getEntityManager();
		
		this.setPadding(new Insets(5));
		
		textArea.setWrapText(true);
		textArea.setText(typicalFragment.getText());
		textArea.textProperty().addListener((observable, oldValue, newValue) -> typicalFragment.setText(newValue));
		setCenter(textArea);
		setRight(createRightPane());
	}
	
	private Pane createRightPane() {		
		GridPane pane = new GridPane();
		
		pane.setPrefWidth(RIGHT_PANEL_WIDTH);
		
		pane.setAlignment(Pos.CENTER);
		pane.setPadding(new Insets(5));
		pane.setHgap(5);
		pane.setVgap(5);
		
		DifferentCharacteristics diffs = model.getDifferentCharacteristics();
		
		List<Partition> partitions = entityManager.createNamedQuery(Partition.READ_ALL, Partition.class).getResultList();
		partitionCombobox.getItems().addAll(partitions);
		Partition selectedPartition = typicalFragment.getQuestion().getPartition();
		partitionCombobox.getSelectionModel().select(selectedPartition);
		partitionCombobox.setMaxWidth(Double.MAX_VALUE);
		partitionCombobox.setButtonCell(new PartitionListCell());
		partitionCombobox.setCellFactory(param -> new PartitionListCell());
		partitionCombobox.setOnAction(event -> {
			questionCombobox.getItems().clear();
			questionCombobox.getItems().addAll(partitionCombobox.getValue().getQuestions());
			questionCombobox.getSelectionModel().selectFirst();
		});
		Label partitionLabel = new Label("������");
		partitionLabel.setMinWidth(80);
		partitionLabel.setAlignment(Pos.CENTER_LEFT);
		if(diffs.isPartition()) {
			partitionLabel.setTextFill(FILL_COLOR);
		}
		alignCell(partitionCombobox, HPos.CENTER);
		alignCell(partitionLabel, HPos.LEFT);
		pane.add(partitionLabel, 0, 0);
		pane.add(partitionCombobox, 1, 0);
		
		questionCombobox.getItems().addAll(selectedPartition.getQuestions());
		questionCombobox.getSelectionModel().select(typicalFragment.getQuestion());
		questionCombobox.setMaxWidth(Double.MAX_VALUE);
		questionCombobox.setButtonCell(new QuestionListCell());
		questionCombobox.setCellFactory(param -> new QuestionListCell());
		questionCombobox.setOnAction(event -> {
			typicalFragment.setQuestion(questionCombobox.getValue());
		});
		Label questionLabel = new Label("������");
		questionLabel.setAlignment(Pos.CENTER_LEFT);
		if(diffs.isQuestion()) {
			questionLabel.setTextFill(FILL_COLOR);
		}
		alignCell(questionCombobox, HPos.CENTER);
		alignCell(questionLabel, HPos.LEFT);
		pane.add(questionLabel, 0, 1);
		pane.add(questionCombobox, 1, 1);
		
		List<Act> acts = entityManager.createNamedQuery(Act.READ_ALL, Act.class).getResultList();
		actCombobox.getItems().addAll(acts);
		actCombobox.getSelectionModel().select(typicalFragment.getAct());
		actCombobox.setMaxWidth(Double.MAX_VALUE);
		actCombobox.setButtonCell(new ActListCell());
		actCombobox.setCellFactory(param -> new ActListCell());
		actCombobox.setOnAction(event -> {
			typicalFragment.setAct(actCombobox.getValue());
		});
		Label actLabel = new Label("���");
		actLabel.setAlignment(Pos.CENTER_LEFT);
		if(diffs.isAct()) {
			actLabel.setTextFill(FILL_COLOR);
		}
		alignCell(actCombobox, HPos.CENTER);
		alignCell(actLabel, HPos.LEFT);
		pane.add(actLabel, 0, 2);
		pane.add(actCombobox, 1, 2);
		
		List<Quality> qualities = entityManager.createNamedQuery(Quality.READ_ALL, Quality.class).getResultList();
		qualityCombobox.getItems().addAll(qualities);
		qualityCombobox.getSelectionModel().selectFirst();
		qualityCombobox.setMaxWidth(Double.MAX_VALUE);
		qualityCombobox.setButtonCell(new QualityListCell());
		qualityCombobox.setCellFactory(param -> new QualityListCell());
		qualityCombobox.setOnAction(event -> {
			typicalFragment.setQuality(qualityCombobox.getValue());
		});
		Label qualityLabel = new Label("��������");
		alignCell(qualityCombobox, HPos.CENTER);
		alignCell(qualityLabel, HPos.LEFT);
		if(diffs.isQuality()) {
			qualityLabel.setTextFill(FILL_COLOR);
		}
		pane.add(qualityLabel, 0, 3);
		pane.add(qualityCombobox, 1, 3);
		
		weightCombobox.getItems().addAll(1.0f, 1.1f, 1.2f, 1.3f, 1.4f, 1.5f);
		weightCombobox.getSelectionModel().selectFirst();
		weightCombobox.setMaxWidth(Double.MAX_VALUE);
		weightCombobox.setOnAction(event -> {
			typicalFragment.setWeight(weightCombobox.getValue());
		});
		Label weightLabel = new Label("���");
		alignCell(weightCombobox, HPos.CENTER);
		alignCell(weightLabel, HPos.LEFT);
		if(diffs.isWeight()) {
			weightLabel.setTextFill(FILL_COLOR);
		}
		pane.add(weightLabel, 0, 4);
		pane.add(weightCombobox, 1, 4);
		
		saveButton.setPrefWidth(250);
		
		saveButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				model.saveTypicalFragment();
			}
		});
		
		alignCell(saveButton, HPos.CENTER);
		pane.add(saveButton, 0, 5, 2, 1);
		
		return pane;
	}

	private void alignCell(Control control, HPos hpos) {
		GridPane.setHalignment(control, hpos);
		GridPane.setValignment(control, VPos.CENTER);
	}
	
	public static final class QualityListCell extends ListCell<Quality> {
		@Override
		protected void updateItem(Quality item, boolean empty) {
			super.updateItem(item, empty);
			if(item != null) {
				setText(item.getCode() + " " + item.getDescription());
			}
		}
	}
	
	public static final class PartitionListCell extends ListCell<Partition> {
		@Override
		protected void updateItem(Partition item, boolean empty) {
			super.updateItem(item, empty);
			if(item != null) {
				setText(item.getCode() + " " + item.getDescription());
			}
		}
	}
	
	public static final class ActListCell extends ListCell<Act> {
		@Override
		protected void updateItem(Act item, boolean empty) {
			super.updateItem(item, empty);
			if(item != null) {
				setText(item.getCode() + " " + item.getName());
			}
		}
	}
	
	public static final class QuestionListCell extends ListCell<Question> {
		@Override
		protected void updateItem(Question item, boolean empty) {
			super.updateItem(item, empty);
			if(item != null) {
				setText(item.getCode() + " " + item.getDescriotion());
			}
		}
	}
}
