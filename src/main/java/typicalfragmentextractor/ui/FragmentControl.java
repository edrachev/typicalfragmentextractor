package typicalfragmentextractor.ui;



import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import typicalfragmentextractor.Model;
import typicalfragmentextractor.persistance.entity.Act;
import typicalfragmentextractor.persistance.entity.Fragment;
import typicalfragmentextractor.persistance.entity.Partition;
import typicalfragmentextractor.persistance.entity.Quality;
import typicalfragmentextractor.persistance.entity.Question;
import typicalfragmentextractor.text.SynticallySplittedFragment;
import typicalfragmentextractor.text.TextUtils;
import typicalfragmentextractor.ui.TypicalFragmentControl.ActListCell;
import typicalfragmentextractor.ui.TypicalFragmentControl.PartitionListCell;
import typicalfragmentextractor.ui.TypicalFragmentControl.QualityListCell;
import typicalfragmentextractor.ui.TypicalFragmentControl.QuestionListCell;

public class FragmentControl extends BorderPane {
	private static final Logger logger = LoggerFactory.getLogger(FragmentControl.class);
	
	private static final int RIGHT_PANEL_WIDTH = 412;
	private static final Color FILL_COLOR = Color.RED;
	
	private final SynticallySplittedFragment<Fragment> fragment;
	private final Model model;
	
	private final TextArea textArea = new TextArea();
	private final ComboBox<Partition> partitionCombobox = new ComboBox<>();
	private final ComboBox<Question> questionCombobox = new ComboBox<>();
	private final ComboBox<Act> actCombobox = new ComboBox<>();
	private final ComboBox<Quality> qualityCombobox = new ComboBox<>();
	private final ComboBox<Float> weightCombobox = new ComboBox<>();
	private final Button removeButton = new Button("������� �� ������");
	
	public FragmentControl(SynticallySplittedFragment<Fragment> fragment, Model model) {
		logger.debug("create; fragment = {}; model = {}", fragment, model);
		this.fragment = fragment;
		this.model = model;
		
		this.setPadding(new Insets(5));
		
		textArea.setWrapText(true);
		textArea.setText(fragment.getFragment().getText());
		textArea.setEditable(false);
		
		setCenter(createText());
		setRight(createRightPane());
	}
	
	private List<String> getDifferentWords() {
		logger.debug("getDifferentWords");
		List<String> typicalWords = TextUtils.toWords(model.getTypicalFragment().getText()).stream().map(el -> el.toLowerCase()).collect(Collectors.toList());
		List<String> differentWords = new ArrayList<String>();
		for(String word: fragment.getWords()) {
			if(!typicalWords.contains(word.toLowerCase())) {
				differentWords.add(word);
			}
		}
		
		logger.debug("differentWords: {}", differentWords);
		return differentWords;
	}
	
	private TextFlow createText() {
		logger.debug("createText");
		List<String> differentWords = getDifferentWords();
		TextFlow textFlow = new TextFlow();
		String text = fragment.getFragment().getText();
		
		Optional<String> closestWord = findClosestToBeginWord(differentWords, text);
		logger.debug("initial text: {}", text);
		while (!text.isEmpty()) {
			if(closestWord.isPresent()) {
				String word = closestWord.get();
				int wordIndex = text.toLowerCase().indexOf(word);
				String firstPart = text.substring(0, wordIndex);
				text = text.substring(wordIndex);
				Text wordText = new Text(text.substring(0, word.length()));
				wordText.setFill(FILL_COLOR);
				textFlow.getChildren().addAll(new Text(firstPart), wordText);
				text = text.substring(word.length());
				logger.debug("text: {}", text);
				closestWord = findClosestToBeginWord(differentWords, text);
			} else {
				logger.debug("last part of text: {}", text);
				textFlow.getChildren().add(new Text(text));
				text = "";
			}
		}
		
		logger.debug("textFlow: {}", textFlow);
		return textFlow;
	}
	
	
	private Optional<String> findClosestToBeginWord(List<String> words, String text) {
		logger.debug("findClosestToBeginWord, words = {}, text = {}", words, text);
		int index = Integer.MAX_VALUE;
		String closestWord = null;
		for(String word: words) {
			int wordIndex = text.toLowerCase().indexOf(word);
			if((wordIndex != -1) && (wordIndex < index)) {
				index = wordIndex;
				closestWord = word;
			}
		}
		
		Optional<String> closestWordOptional = Optional.ofNullable(closestWord);
		logger.debug("closestWordOptional: {}", closestWordOptional);
		return closestWordOptional;
	}
	
	private Pane createRightPane() {		
		GridPane pane = new GridPane();
		pane.setPrefWidth(RIGHT_PANEL_WIDTH);
		
		pane.setAlignment(Pos.CENTER);
		pane.setPadding(new Insets(5));
		pane.setHgap(5);
		pane.setVgap(5);
		
		Partition partition = fragment.getFragment().getQuestion().getPartition();
		partitionCombobox.getItems().addAll(partition);
		partitionCombobox.getSelectionModel().select(partition);
		partitionCombobox.setMaxWidth(Double.MAX_VALUE);
		partitionCombobox.setButtonCell(new PartitionListCell());
		partitionCombobox.setCellFactory(param -> new PartitionListCell());
		partitionCombobox.setDisable(true);
		partitionCombobox.setOpacity(1);
		Label partitionLabel = new Label("������");
		partitionLabel.setAlignment(Pos.CENTER_LEFT);
		if(!fragment.getFragment().getQuestion().getPartition().equals(model.getTypicalFragment().getQuestion().getPartition())) {
			partitionLabel.setTextFill(FILL_COLOR);
		}
		partitionLabel.setMinWidth(80);
		alignCell(partitionCombobox, HPos.CENTER);
		alignCell(partitionLabel, HPos.LEFT);
		pane.add(partitionLabel, 0, 0);
		pane.add(partitionCombobox, 1, 0);
		
		questionCombobox.getItems().addAll(fragment.getFragment().getQuestion());
		questionCombobox.getSelectionModel().select(fragment.getFragment().getQuestion());
		questionCombobox.setMaxWidth(Double.MAX_VALUE);
		questionCombobox.setButtonCell(new QuestionListCell());
		questionCombobox.setCellFactory(param -> new QuestionListCell());
		questionCombobox.setDisable(true);
		questionCombobox.setOpacity(1);
		Label questionLabel = new Label("������");
		questionLabel.setAlignment(Pos.CENTER_LEFT);
		if(!fragment.getFragment().getQuestion().equals(model.getTypicalFragment().getQuestion())) {
			questionLabel.setTextFill(FILL_COLOR);
		}
		alignCell(questionCombobox, HPos.CENTER);
		alignCell(questionLabel, HPos.LEFT);
		pane.add(questionLabel, 0, 1);
		pane.add(questionCombobox, 1, 1);
		
		actCombobox.getItems().addAll(fragment.getFragment().getAct());
		actCombobox.getSelectionModel().select(fragment.getFragment().getAct());
		actCombobox.setMaxWidth(Double.MAX_VALUE);
		actCombobox.setButtonCell(new ActListCell());
		actCombobox.setCellFactory(param -> new ActListCell());
		actCombobox.setDisable(true);
		actCombobox.setOpacity(1);
		Label actLabel = new Label("���");
		actLabel.setAlignment(Pos.CENTER_LEFT);
		if(!fragment.getFragment().getAct().equals(model.getTypicalFragment().getAct())) {
			actLabel.setTextFill(FILL_COLOR);
		}
		alignCell(actCombobox, HPos.CENTER);
		alignCell(actLabel, HPos.LEFT);
		pane.add(actLabel, 0, 2);
		pane.add(actCombobox, 1, 2);
		
		qualityCombobox.getItems().addAll(fragment.getFragment().getQuality());
		qualityCombobox.getSelectionModel().select(fragment.getFragment().getQuality());
		qualityCombobox.setMaxWidth(Double.MAX_VALUE);
		qualityCombobox.setButtonCell(new QualityListCell());
		qualityCombobox.setCellFactory(param -> new QualityListCell());
		qualityCombobox.setDisable(true);
		qualityCombobox.setOpacity(1);
		Label qualityLabel = new Label("��������");
		alignCell(qualityCombobox, HPos.CENTER);
		if(!fragment.getFragment().getQuality().equals(model.getTypicalFragment().getQuality())) {
			qualityLabel.setTextFill(FILL_COLOR);
		}
		alignCell(qualityLabel, HPos.LEFT);
		pane.add(qualityLabel, 0, 3);
		pane.add(qualityCombobox, 1, 3);
		
		weightCombobox.getItems().addAll(1.0f, 1.1f, 1.2f, 1.3f, 1.4f, 1.5f);
		weightCombobox.getSelectionModel().selectFirst();
		weightCombobox.setMaxWidth(Double.MAX_VALUE);
		weightCombobox.setDisable(true);
		weightCombobox.setOpacity(1);
		Label weightLabel = new Label("���");
		alignCell(weightCombobox, HPos.CENTER);
		if(!fragment.getFragment().getWeight().equals(model.getTypicalFragment().getWeight())) {
			weightLabel.setTextFill(FILL_COLOR);
		}
		alignCell(weightLabel, HPos.LEFT);
		pane.add(weightLabel, 0, 4);
		pane.add(weightCombobox, 1, 4);
		
		removeButton.setPrefWidth(150);
		
		removeButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				model.removeFragmentFromGroup(fragment);
			}
		});
		
		alignCell(removeButton, HPos.CENTER);
		pane.add(removeButton, 0, 5, 2, 1);
		
		return pane;
	}

	private void alignCell(Control control, HPos hpos) {
		GridPane.setHalignment(control, hpos);
		GridPane.setValignment(control, VPos.CENTER);
	}
}
